package ru.dolbak.iss

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.geometry.Polyline
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.PlacemarkMapObject
import com.yandex.mapkit.mapview.MapView
import com.yandex.runtime.image.ImageProvider
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.lang.Exception


class MainActivity : AppCompatActivity() {

    lateinit var mapView: MapView
    lateinit var placemark: PlacemarkMapObject
    var prevLat = -10000.0
    var prevLon = -10000.0


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        MapKitFactory.setApiKey("495855bd-5631-4993-ac9a-caca31f0cfff")
        MapKitFactory.initialize(this)

        setContentView(R.layout.activity_main)

        mapView = findViewById<View>(R.id.mapview) as MapView


        val tv = findViewById<TextView>(R.id.textView)

        val retrofit = Retrofit.Builder()
            .baseUrl("https://api.wheretheiss.at/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val issService = retrofit.create(ISSLocationService::class.java)

        val thread = Thread{
            while (true){
                try{
                    val response = issService.getLocation().execute()
                    ISSLocationViewModel.getInstance().myData.postValue(response.body())
                }
                catch (e: Exception){
                    Log.d("NIKITA", "Проблемы с интернетом")
                }
                Thread.sleep(2000)
            }
        }.start()

        ISSLocationViewModel.getInstance().myData.observe(this, Observer {
            val lat = it.latitude.toString().toDouble()
            val lon = it.longitude.toString().toDouble()
            Log.d("NIKITA", "$lat $lon")
            mapView.map.move(
                CameraPosition(Point(lat, lon), 7.0f, 0.0f, 0.0f),
                Animation(Animation.Type.SMOOTH, 0F),
                null
            )
            if (this::placemark.isInitialized){
                mapView.map.mapObjects.remove(placemark)
            }
            placemark = mapView.map.mapObjects.addPlacemark(Point(lat, lon), ImageProvider.fromResource(this, R.drawable.icon))

            if (prevLat != -10000.0 && prevLon != -10000.0){
                mapView.map.mapObjects.addPolyline(Polyline(listOf(Point(lat, lon), Point(prevLat, prevLon))))
            }
            tv.text = String.format("lat: %.4f lon: %.4f", lat, lon)
            prevLat = lat
            prevLon = lon
        })

    }
    override fun onStop() {
        mapView.onStop()
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
        mapView.onStart()
    }
}