package ru.dolbak.iss

import com.google.gson.annotations.SerializedName

data class ISSLocation (
    @SerializedName("name"       ) var name       : String? = null,
    @SerializedName("id"         ) var id         : Int?    = null,
    @SerializedName("latitude"   ) var latitude   : Double? = null,
    @SerializedName("longitude"  ) var longitude  : Double? = null,
    @SerializedName("altitude"   ) var altitude   : Double? = null,
    @SerializedName("velocity"   ) var velocity   : Double? = null,
    @SerializedName("visibility" ) var visibility : String? = null,
    @SerializedName("footprint"  ) var footprint  : Double? = null,
    @SerializedName("timestamp"  ) var timestamp  : Int?    = null,
    @SerializedName("daynum"     ) var daynum     : Double? = null,
    @SerializedName("solar_lat"  ) var solarLat   : Double? = null,
    @SerializedName("solar_lon"  ) var solarLon   : Double? = null,
    @SerializedName("units"      ) var units      : String? = null

)