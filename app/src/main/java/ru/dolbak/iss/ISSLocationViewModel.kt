package ru.dolbak.iss

import androidx.lifecycle.MutableLiveData

class ISSLocationViewModel private constructor() {
    companion object {

        @Volatile
        private var instance: ISSLocationViewModel? = null

        fun getInstance() =
            instance ?: synchronized(this) {
                instance ?: ISSLocationViewModel().also { instance = it }
            }
    }

    val myData = MutableLiveData<ISSLocation>()
}