package ru.dolbak.iss

import retrofit2.Call
import retrofit2.http.GET

interface ISSLocationService {
    @GET("v1/satellites/25544")
    fun getLocation() : Call<ISSLocation>
}